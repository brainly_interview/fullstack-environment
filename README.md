# This might be useful to confirm, that your environment is ready for an interview

# Backend part
There are two different backend options:
* Python and Flask
* JavaScript and Express
* Java with Spark

Please check if you are able to setup a server in prefered language.

# Python

Create virtualenv:
* python3 -m venv env
* source env/bin/activate
* pip3 install -r requirements.txt
* python3 app.py

# JavaScript
* yarn
* yarn start

# Java
* mvn clean install package
* mvn exec:java

# Frontend

Please check if you are able to setup a react development server.

## How to start server?
* yarn (npm install)
* yarn start (npm run start)
