import com.google.gson.Gson;
import spark.Request;
import spark.Response;
import spark.ResponseTransformer;
import spark.Route;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;


public class Server {
    public static void main(String[] args) {
        port(3001);
        get("/", fetchCustomers, new JsonTransformer());
    }

    public static Route fetchCustomers = (Request request, Response response) -> {
        List<String> list = new ArrayList<String>();

        Connection connection = DriverManager.getConnection("jdbc:sqlite:Chinook_Sqlite_AutoIncrementPKs.sqlite");
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT City FROM Customer");

        while(rs.next())
        {
            list.add(rs.getString("City"));
        }

        return list;
    };

    private static class JsonTransformer implements ResponseTransformer {
        private final Gson gson = new Gson();

        @Override
        public String render(Object model) {
            return gson.toJson(model);
        }
    }
}