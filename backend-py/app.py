from flask import Flask, g
from flask_restful import Resource, Api
import sqlite3
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
api = Api(app)

DB_NAME = 'Chinook_Sqlite_AutoIncrementPKs.sqlite'


def get_db():
    db = getattr(g, '_database', None)

    if db is None:
        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d
        db = g._database = sqlite3.connect(DB_NAME)
        db.row_factory = dict_factory
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def query_db(query, args=(), one=False):
    print(query)
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


class Get(Resource):
    def get(self):
        SELECT = ("SELECT * FROM Customer")
        return query_db(SELECT)


api.add_resource(Get, '/')


if __name__ == "__main__":
    app.run(debug=True, port=3001)
